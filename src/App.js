import "./App.css";
import Navbar from "./component/Navbar";
import Main from "./component/Main";
import Text from "./component/Text";
import Card from "./component/Card";

import data from "./data";

function App() {
  const cards = data.map((userData) => {
    return (
      <Card
        img={userData.Img}
        rating={userData.stats.rating}
        reviewcount={userData.stats.reviewCount}
        country={userData.location}
        availability={userData.openSpots}
        title={userData.title}
        price={userData.price}
        openSpots={userData.openSpots}
      />
    );
  });

  return (
    <div className="App">
      <Navbar />
      <Main />
      <Text />
      <div className="Experiences">{cards}</div>
    </div>
  );
}

export default App;
