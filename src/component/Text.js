import React from "react";
import "./Text.css";

export default function Text() {
  return (
    <div className="textContainer">
      <div className="text">
        <h1>online Experiences</h1>

        <p className="para">
          Join unique interactive activities led by one-of-a-kind hosts—all
          without leaving home.
        </p>
      </div>
    </div>
  );
}
