import React from "react";
import "./Card.css";

export default function Card(prop) {
  let status;
  if (prop.openSpots === 0) {
    status = "SOLD OUT";
  } else if (prop.country === "Online") {
    status = "ONLINE";
  }
  return (
    <div className="card">
      {status && <div className="status">{status}</div>}
      <img src={prop.img} className="card-img" alt="persons-img"></img>
      <div className="rating">
        <img src="images/Star 1.png" alt="star-img"></img>
        <span> {prop.rating}</span>
        <div className="grayscale">
          <span className="layeredtext">({prop.availability})</span>
          <span className="layeredtext">.{prop.country}</span>
        </div>
      </div>

      <p className="card-title">{prop.title}</p>
      <p>
        <span className="boldtext">
          <b>From ${prop.price}</b> /person
        </span>
      </p>
    </div>
  );
}
